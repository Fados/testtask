//connecting cron library and setting time intervals for sending messages
const { channel } = require('diagnostics_channel');
var cron = require('node-cron');

cron.schedule('0 0 * * 0-6', async() => {
    //making request on given http
    const url = 'http://test.test/?getbd'
    const responseJSON = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    })
    if (responseJSON.ok) {
        //if response reached us,we need to find out,what userId has user with given email
        let json = await responseJSON.json();
        //user token is random(may be bot token)
        const token = 'xoxp-*******'
        const channelName = 'someChannel'
        console.log(json)
            //making requests for all supervisors
        json.forEach(async element => {
            let userEmail = element.rukovoditel;
            //from user mail with thit token would be sent messages,that were given in task
            const responseEmail = await fetch(`https: //slack.com/api/users.lookupByEmail?token=${token}&email=${userEmail}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                }
            })
            if (responseEmail.ok) {
                //finally we have userId from higher request and now we can send mail
                const responseSendMail = await fetch('https://slack.com/api/chat.postEphemeral', {
                    method: 'POST',
                    //channel name is random
                    body: JSON.stringify({ channel: channelName, user: data2.user.id, text: element.sotrudniki }),
                    headers: {
                        //user token is random(may be bot token)
                        'token': token,
                        'Content-Type': 'application/json;charset=utf-8'
                    }
                })
                if (responseSendMail.ok) {
                    console.log(`Message to ${userEmail} delivered  successfully`)
                } else {
                    throw new Error(`Message to ${userEmail} wasn't delivered`)
                }
            } else {
                alert(`User with email ${userEmail} not found`);
            }
        })

    } else {
        throw new Error(`No response JSON,status ${responseJSON.status}`)

    }
})